// src/store.js
import { createStore } from 'vuex';

export default createStore({
  state: {
    tasks: [
      { id: 1, title: 'Tâche 1', completed: false },
      { id: 2, title: 'Tâche 2', completed: false }
    ]
  },
  mutations: {
    addTask(state, title) {
      const id = state.tasks.length + 1;
      state.tasks.push({ id, title, completed: false });
    },
    deleteTask(state, id) {
      state.tasks = state.tasks.filter(task => task.id !== Number(id));
    }
  },
  actions: {
    addTask({ commit }, title) {
      commit('addTask', title);
    },
    deleteTask({ commit }, id) {
      commit('deleteTask', id);
    }
  },
  getters: {
    tasks: (state) => state.tasks
  }
});
