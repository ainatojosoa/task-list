
import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import ContactView from '../../views/ContactView.vue'

describe('ContactView', () => {
  it('Voir s\'il y a un titre dans contact', () => {
    const wrapper = mount(ContactView)

    expect(wrapper.find('h2').exists()).toBe(true)
    })
})