import axios from "axios";

//Configuration par défaut pour pouvoir utiliser axios et fractionner le code

export default axios.create({
  baseURL: "https://jsonplaceholder.typicode.com",
  headers: {
    "Content-type": "application/json"
  }
});