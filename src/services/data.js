import http from "./http-common";

class DataService {
  getAll() {
    return http.get("/posts");
  }

}

export default new DataService();